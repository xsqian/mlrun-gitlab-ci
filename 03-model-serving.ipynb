{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Part 3: Serving an ML Model\n",
    "\n",
    "This part of the MLRun getting-started tutorial walks you through the steps for implementing ML model serving using MLRun serving and Nuclio runtimes.\n",
    "The tutorial walks you through the steps for creating, deploying, and testing a model-serving function (\"a serving function\" a.k.a. \"a model server\").\n",
    "\n",
    "MLRun serving can produce managed real-time serverless pipelines from various tasks, including MLRun models or standard model files.\n",
    "The pipelines use the Nuclio real-time serverless engine, which can be deployed anywhere.\n",
    "[Nuclio](https://nuclio.io/) is a high-performance open-source \"serverless\" framework that's focused on data, I/O, and compute-intensive workloads.\n",
    "\n",
    "Simple model serving classes can be written in Python or be taken from a set of pre-developed ML/DL classes.\n",
    "The code can handle complex data, feature preparation, and binary data (such as images and video files).\n",
    "The Nuclio serving engine supports the full model-serving life cycle;\n",
    "this includes auto generation of microservices, APIs, load balancing, model logging and monitoring, and configuration management.\n",
    "\n",
    "MLRun serving supports more advanced real-time data processing and model serving pipelines.\n",
    "For more details and examples, see the [MLRun Serving Graphs](../serving/serving-graph.md) documentation.\n",
    "\n",
    "The tutorial consists of the following steps:\n",
    "\n",
    "1. [Setup and Configuration](#gs-tutorial-3-step-setup) &mdash; load your project\n",
    "2. [Writing A Simple Serving Class](#gs-tutorial-3-step-writing-a-serving-class)\n",
    "3. [Deploying the Model-Serving Function (Service)](#gs-tutorial-3-step-deploy-the-serving-function)\n",
    "4. [Using the Live Model-Serving Function](#gs-tutorial-3-step-using-the-live-serving-function)\n",
    "5. [Viewing the Nuclio Serving Function on the Dashboard](#gs-tutorial-3-step-view-serving-func-in-ui)\n",
    "\n",
    "By the end of this tutorial you'll learn how to\n",
    "\n",
    "- Create model-serving functions.\n",
    "- Deploy models at scale.\n",
    "- Test your deployed models."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"gs-tutorial-3-prerequisites\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Prerequisites\n",
    "\n",
    "The following steps are a continuation of the previous parts of this getting-started tutorial and rely on the generated outputs.\n",
    "Therefore, make sure to first run parts 1&mdash;[2](02-model-training.ipynb) of the tutorial."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"gs-tutorial-3-step-setup\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Step 1: Setup and Configuration"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"gs-tutorial-3-import-libaries\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Importing Libraries\n",
    "\n",
    "Run the following code to import required libraries:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import mlrun"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"gs-tutorial-4-set-mlrun-envr\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Initializing Your MLRun Environment\n",
    "\n",
    "Use the `get_or_create_project` MLRun method to create a new project or fetch it from the DB/repository if it already exists.\n",
    "Set the `project` and `user_project` parameters to the same values that you used in the call to this method in the [Part 1: MLRun Basics](./01-mlrun-basics.ipynb#gs-tutorial-1-mlrun-envr-init) tutorial notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "> 2023-01-29 05:08:20,344 [info] loaded project mlrun-gitlab-ci from MLRun DB\n"
     ]
    }
   ],
   "source": [
    "# Set the base project name\n",
    "project_name_base = 'mlrun-gitlab-ci'\n",
    "\n",
    "# Initialize the MLRun project object\n",
    "project = mlrun.get_or_create_project(project_name_base, context=\"./\", user_project=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"gs-tutorial-3-step-writing-a-serving-class\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Step 2: Writing A Simple Serving Class\n",
    "\n",
    "The serving class is initialized automatically by the model server.\n",
    "All you need is to implement two mandatory methods:\n",
    "\n",
    "- `load` &mdash; downloads the model files and loads the model into memory.\n",
    "    This can be done either synchronously or asynchronously.\n",
    "- `predict` &mdash; accepts a request payload and returns prediction (inference) results.\n",
    "\n",
    "For more detailed information on serving classes, see the [MLRun documentation](https://github.com/mlrun/mlrun/blob/release/v0.6.x-latest/mlrun/serving/README.md).\n",
    "\n",
    "The following code demonstrates a minimal scikit-learn (a.k.a. sklearn) serving-class implementation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "# mlrun: start-code"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cloudpickle import load\n",
    "import numpy as np\n",
    "from typing import List\n",
    "import mlrun\n",
    "from os import environ\n",
    "\n",
    "class ClassifierModel(mlrun.serving.V2ModelServer):\n",
    "    def load(self):\n",
    "        \"\"\"load and initialize the model and/or other elements\"\"\"\n",
    "        model_file, extra_data = self.get_model('.pkl')\n",
    "        self.model = load(open(model_file, 'rb'))\n",
    "        print(f'extra_data = {extra_data}')\n",
    "        print(f'self.get_param = {self.get_param(\"test_set\")}')\n",
    "        print(f'self._params = {self._params}')\n",
    "        environ[\"KEY1\"] = self.context.get_secret(\"KEY1\")\n",
    "        print(f'KEY1  = {environ[\"KEY1\"]}')\n",
    "    def predict(self, body: dict) -> List:\n",
    "        \"\"\"Generate model predictions from sample.\"\"\"\n",
    "        feats = np.asarray(body['inputs'])\n",
    "        result: np.ndarray = self.model.predict(feats)\n",
    "        return result.tolist()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "# mlrun: end-code"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"gs-tutorial-3-step-deploy-the-serving-function\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Step 3: Deploying the Model-Serving Function (Service)\n",
    "\n",
    "To provision (deploy) a function for serving the model (\"a serving function\") you need to create an MLRun function of type `serving`.\n",
    "You can do this by using the `code_to_function` MLRun method from a web notebook, or by importing an existing serving function or template from the MLRun functions marketplace."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"gs-tutorial-3-convert-serving-class-to-function\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Converting a Serving Class to a Serving Function\n",
    "\n",
    "The following code converts the `ClassifierModel` class that you defined in the previous step to a serving function.\n",
    "The name of the class to be used by the serving function is set in `spec.default_class`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "serving_fn = mlrun.code_to_function('serving', kind='serving',image='mlrun/mlrun')\n",
    "serving_fn.spec.default_class = 'ClassifierModel'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Add the model created in previous notebook by the training function  \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "store://artifacts/mlrun-gitlab-ci-xingsheng/my_model\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "<mlrun.serving.states.TaskStep at 0x7f38f8b42090>"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "model_file = project.get_artifact_uri('my_model') \n",
    "print(model_file)\n",
    "serving_fn.add_model('my_model',model_path=model_file)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"gs-tutorial-3-test-func-locally\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Testing Your Function Locally\n",
    "\n",
    "To test your function locally, create a test server (mock server) and test it with sample data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    },
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "my_data = '''{\"inputs\":[[5.1, 3.5, 1.4, 0.2],[7.7, 3.8, 6.7, 2.2]]}'''"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    },
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "extra_data = {'feature-importance': 'v3io:///projects/mlrun-gitlab-ci-xingsheng/artifacts/train-iris-train_iris/0/feature-importance.html', 'test_set': 'v3io:///projects/mlrun-gitlab-ci-xingsheng/artifacts/train-iris-train_iris/0/test_set.parquet', 'confusion-matrix': 'v3io:///projects/mlrun-gitlab-ci-xingsheng/artifacts/train-iris-train_iris/0/confusion-matrix.html', 'roc-curves': 'v3io:///projects/mlrun-gitlab-ci-xingsheng/artifacts/train-iris-train_iris/0/roc-curves.html', 'validation-confusion matrix table.csv': 'v3io:///projects/mlrun-gitlab-ci-xingsheng/artifacts/train-iris-train_iris/0/model/validation-confusion matrix table.csv', 'validation-confusion matrix': 'v3io:///projects/mlrun-gitlab-ci-xingsheng/artifacts/plots/test/0/confusion-matrix.html', 'validation-feature importances': 'v3io:///projects/mlrun-gitlab-ci-xingsheng/artifacts/plots/test/0/feature-importances.html', 'validation-feature importances table.csv': 'v3io:///projects/mlrun-gitlab-ci-xingsheng/artifacts/train-iris-train_iris/0/model/validation-feature importances table.csv', 'validation-precision_recall_multi': 'v3io:///projects/mlrun-gitlab-ci-xingsheng/artifacts/plots/test/0/precision-recall-multiclass.html', 'validation-roc_multi': 'v3io:///projects/mlrun-gitlab-ci-xingsheng/artifacts/plots/test/0/roc-multiclass.html'}\n",
      "self.get_param = None\n",
      "self._params = {'full_event': None}\n",
      "KEY1  = V1\n",
      "> 2023-01-29 05:08:39,092 [info] model my_model was loaded\n",
      "> 2023-01-29 05:08:39,093 [info] Loaded ['my_model']\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "X does not have valid feature names, but RandomForestClassifier was fitted with feature names\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "{'id': '909b160012ee4fe68fdd4f0389f3cee4',\n",
       " 'model_name': 'my_model',\n",
       " 'outputs': [0, 2]}"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "serving_fn.with_secrets(kind=\"file\", source=\"config.sec\")\n",
    "server = serving_fn.to_mock_server()\n",
    "server.test(\"/v2/models/my_model/infer\", body=my_data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"gs-tutorial-3-building-and-deploying-the-serving-function\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Building and Deploying the Serving Function\n",
    "\n",
    "Use the `deploy` method of the MLRun serving function to build and deploy a Nuclio serving function from your serving-function code."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "> 2023-01-29 05:08:39,111 [info] Starting remote function deploy\n",
      "2023-01-29 05:08:39  (info) Deploying function\n",
      "2023-01-29 05:08:39  (info) Building\n",
      "2023-01-29 05:08:39  (info) Staging files and preparing base images\n",
      "2023-01-29 05:08:39  (info) Building processor image\n",
      "2023-01-29 05:09:24  (info) Build complete\n",
      "2023-01-29 05:09:32  (info) Function deploy complete\n",
      "> 2023-01-29 05:09:34,134 [info] successfully deployed function: {'internal_invocation_urls': ['nuclio-mlrun-gitlab-ci-xingsheng-serving.default-tenant.svc.cluster.local:8080'], 'external_invocation_urls': ['mlrun-gitlab-ci-xingsheng-serving-mlrun-gitlab-ci-xingsheng.default-tenant.app.us-sales-350.iguazio-cd1.com/']}\n"
     ]
    }
   ],
   "source": [
    "function_address = serving_fn.deploy()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"gs-tutorial-3-step-using-the-live-serving-function\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Step 4: Using the Live Model-Serving Function\n",
    "\n",
    "After the function is deployed successfully, the serving function has a new HTTP endpoint for handling serving requests.\n",
    "The example tutorial serving function receives HTTP prediction (inference) requests on this endpoint;\n",
    "calls the `infer` method to get the requested predictions; and returns the results on the same endpoint."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The address for the function is http://mlrun-gitlab-ci-xingsheng-serving-mlrun-gitlab-ci-xingsheng.default-tenant.app.us-sales-350.iguazio-cd1.com/ \n",
      "\n",
      "{\"name\": \"ModelRouter\", \"version\": \"v2\", \"extensions\": []}"
     ]
    }
   ],
   "source": [
    "print (f'The address for the function is {function_address} \\n')\n",
    "\n",
    "!curl $function_address"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"gs-tutorial-3-testing-the-model-server\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Testing the Model Server\n",
    "\n",
    "Test your model server by sending data for inference.\n",
    "The `invoke` serving-function method enables programmatic testing of the serving function.\n",
    "For model inference (predictions), specify the model name followed by `infer`:\n",
    "```\n",
    "/v2/models/{model_name}/infer\n",
    "```\n",
    "For complete model-service API commands &mdash; such as for list models (`models`), get model health (`ready`), and model explanation (`explain`) &mdash; see the [MLRun documentation](https://github.com/mlrun/mlrun/blob/release/v0.6.x-latest/mlrun/serving/README.md#model-server-api)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "> 2023-01-29 05:09:35,100 [info] invoking function: {'method': 'POST', 'path': 'http://nuclio-mlrun-gitlab-ci-xingsheng-serving.default-tenant.svc.cluster.local:8080/v2/models/my_model/infer'}\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "{'id': 'efddb309-63d6-45b2-a4c9-5e3e91eee109',\n",
       " 'model_name': 'my_model',\n",
       " 'outputs': [0, 2]}"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "serving_fn.invoke('/v2/models/my_model/infer', my_data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"gs-tutorial-3-step-view-serving-func-in-ui\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Step 5: Viewing the Nuclio Serving Function on the Dashboard\n",
    "\n",
    "On the **Projects** dashboard page, select the project and then select \"Real-time functions (Nuclio)\".\n",
    "\n",
    "![Nuclio](./images/nuclio-deploy.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"gs-tutorial-3-done\"></a>\n",
    "\n",
    "## Done!\n",
    "\n",
    "Congratulation! You've completed Part 3 of the MLRun getting-started tutorial.\n",
    "Proceed to [**Part 4: ML Pipeline**](04-pipeline.ipynb) to learn how to create an automated pipeline for your project."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python [conda env:root] *",
   "language": "python",
   "name": "conda-root-py"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
